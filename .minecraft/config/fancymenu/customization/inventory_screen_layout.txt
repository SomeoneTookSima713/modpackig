type = fancymenu_layout

layout-meta {
  identifier = inventory_screen
  render_custom_elements_behind_vanilla = false
  last_edited_time = 1713630613597
  is_enabled = true
  randommode = false
  randomgroup = 1
  randomonlyfirsttime = false
  layout_index = 0
  [loading_requirement_container_meta:e80821b3-be1b-4066-9525-904c20a7c646-1713630564943] = [groups:][instances:]
}

customization {
  action = backgroundoptions
  keepaspectratio = false
}

scroll_list_customization {
  preserve_scroll_list_header_footer_aspect_ratio = true
  render_scroll_list_header_shadow = true
  render_scroll_list_footer_shadow = true
  show_scroll_list_header_footer_preview_in_editor = false
  repeat_scroll_list_header_texture = false
  repeat_scroll_list_footer_texture = false
}

vanilla_button {
  button_element_executable_block_identifier = 60dfcae2-2e58-46fd-966b-e4d085e3d614-1713630564945
  [executable_block:60dfcae2-2e58-46fd-966b-e4d085e3d614-1713630564945][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 0
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = top-left
  x = 12
  y = 56
  width = 70
  height = 36
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 522c3ebd-7f9a-421d-97cc-55c9da5030f6-1713630564945
  [loading_requirement_container_meta:522c3ebd-7f9a-421d-97cc-55c9da5030f6-1713630564945] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

vanilla_button {
  button_element_executable_block_identifier = 334e5581-2f27-45f8-a15a-bbd516bc4567-1713630564945
  [executable_block:334e5581-2f27-45f8-a15a-bbd516bc4567-1713630564945][type:generic] = [executables:]
  restartbackgroundanimations = true
  loopbackgroundanimations = true
  nine_slice_custom_background = false
  nine_slice_border_x = 5
  nine_slice_border_y = 5
  navigatable = true
  element_type = vanilla_button
  instance_identifier = 516478
  appearance_delay = no_delay
  appearance_delay_seconds = 1.0
  fade_in = false
  fade_in_speed = 1.0
  anchor_point = vanilla
  x = 336
  y = 146
  width = 20
  height = 18
  stretch_x = false
  stretch_y = false
  stay_on_screen = true
  element_loading_requirement_container_identifier = 256a61e7-c376-43d3-a6e0-fbb52fc7cd43-1713630564945
  [loading_requirement_container_meta:256a61e7-c376-43d3-a6e0-fbb52fc7cd43-1713630564945] = [groups:][instances:]
  is_hidden = false
  automated_button_clicks = 0
  nine_slice_slider_handle = false
  nine_slice_slider_handle_border_x = 5
  nine_slice_slider_handle_border_y = 5
}

