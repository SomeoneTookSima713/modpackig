// Mechanic: Igniting candles and campfires with burning items
BlockEvents.rightClicked(event => {
    const BURNING = [
        "minecraft:torch",
        "minecraft:soul_torch",
        "minecraft:fire_charge"
    ];
    const VALID = [
        new RegExp(".+_candle"),
        new RegExp(".+_?campfire")
    ]
	let holding = false;
    for (let b of BURNING) {
        holding = holding || event.getPlayer().isHolding(b);
    }
    if (holding && VALID.find((value, _ind, _obj) => event.block.id.match(value) !== null) !== undefined) {
        let id = event.getBlock().getId();
        let state = event.getBlock().properties;
        state.put("lit", "true");
        event.getBlock().set(Block.id(id), state);
        let player = event.player.getName().getString();
        let pos = event.getBlock().getPos();
        let cmd = `execute at ${player} run playsound minecraft:item.firecharge.use block @a ${pos.x} ${pos.y} ${pos.z} 0.15`;
        event.server.runCommandSilent(cmd);
        event.success();
    }
})