const DEBUG = false;



const AttributeModifier = Java.loadClass("net.minecraft.world.entity.ai.attributes.AttributeModifier");
const AttributeModifierOperation = Java.loadClass("net.minecraft.world.entity.ai.attributes.AttributeModifier$Operation");


const TERRASTEEL_NERF_UUID = UUID.fromString("4f7a0d3b-2053-4ad2-87de-8230555f8717");
const TERRASTEEL_NERF = new AttributeModifier(TERRASTEEL_NERF_UUID, "modpack:terrasteel_nerf", -0.5, AttributeModifierOperation.MULTIPLY_BASE);

function addPermanentModifierSafe(attr, modifier) {
    if (!attr.hasModifier(modifier)) {
        attr.addPermanentModifier(modifier);
    }
}

function removePermanentModifierSafe(attr, modifier) {
    if (attr.hasModifier(modifier)) {
        attr.removePermanentModifier(modifier.getId());
    }
}

EntityEvents.hurt(event => {
    if (!DEBUG) { return; }

    let source = event.source;
    
    if (source.getType() == "player") {
        let player = event.source.player;
        player.setStatusMessage(event.damage.toString());
    }
});

PlayerEvents.inventoryChanged(event => {
    let attack_damage_attr = event.player.getAttribute("generic.attack_damage");

    let ancient_will_verac = event.player.headArmorItem.nbt.get("AncientWill_verac") ?? NBT.byteTag(0);

    if (event.player.headArmorItem.id == "botania:terrasteel_helmet" && ancient_will_verac.getAsString().startsWith("1")) {
        addPermanentModifierSafe(attack_damage_attr, TERRASTEEL_NERF);
    } else if (attack_damage_attr.hasModifier(TERRASTEEL_NERF)) {
        removePermanentModifierSafe(attack_damage_attr, TERRASTEEL_NERF);
    }
});