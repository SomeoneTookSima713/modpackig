ServerEvents.recipes(event => {
    event.remove({output: "techreborn:quantum_helmet"});
    event.remove({output: "techreborn:quantum_chestplate"});
    event.remove({output: "techreborn:quantum_leggings"});
    event.remove({output: "techreborn:quantum_boots"});

    event.shaped("techreborn:quantum_helmet", [
        'DLD',
        'SQS',
        '   '
    ], {
        D: "techreborn:data_storage_chip",
        L: "techreborn:lapotron_crystal",
        S: "techreborn:superconductor",
        Q: "ae2:singularity"
    });

    event.shaped("techreborn:quantum_chestplate", [
        'TQT',
        'SLS',
        'DID'
    ], {
        D: "techreborn:data_storage_chip",
        L: "techreborn:lapotron_crystal",
        S: "techreborn:superconductor",
        Q: "ae2:singularity",
        T: "techreborn:tungstensteel_plate",
        I: "techreborn:iridium_neutron_reflector"
    });

    event.shaped("techreborn:quantum_leggings", [
        'DLD',
        'SQS',
        'T T'
    ], {
        D: "techreborn:data_storage_chip",
        L: "techreborn:lapotron_crystal",
        S: "techreborn:superconductor",
        Q: "ae2:singularity",
        T: "techreborn:tungstensteel_plate"
    });

    event.shaped("techreborn:quantum_boots", [
        'L L',
        'DQD',
        'S S'
    ], {
        D: "techreborn:data_storage_chip",
        L: "techreborn:lapotron_crystal",
        S: "techreborn:superconductor",
        Q: "ae2:singularity"
    });



    event.remove({ output: "wizards:arcane_robe_head" });
    event.remove({ output: "wizards:arcane_robe_chest" });
    event.remove({ output: "wizards:arcane_robe_legs" });
    event.remove({ output: "wizards:arcane_robe_feet" });

    event.remove({ output: "wizards:fire_robe_head" });
    event.remove({ output: "wizards:fire_robe_chest" });
    event.remove({ output: "wizards:fire_robe_legs" });
    event.remove({ output: "wizards:fire_robe_feet" });

    event.remove({ output: "wizards:frost_robe_head" });
    event.remove({ output: "wizards:frost_robe_chest" });
    event.remove({ output: "wizards:frost_robe_legs" });
    event.remove({ output: "wizards:frost_robe_feet" });

    event.shaped("wizards:arcane_robe_head",[
        ' A ',
        'EWE',
        ' A '
    ], {
        W: "wizards:wizard_robe_head",
        E: "minecraft:ender_pearl",
        A: "minecraft:amethyst_shard"
    });

    event.shaped("wizards:arcane_robe_chest",[
        ' A ',
        'EWE',
        ' A '
    ], {
        W: "wizards:wizard_robe_chest",
        E: "minecraft:ender_pearl",
        A: "minecraft:amethyst_shard"
    });

    event.shaped("wizards:arcane_robe_legs",[
        ' A ',
        'EWE',
        ' A '
    ], {
        W: "wizards:wizard_robe_legs",
        E: "minecraft:ender_pearl",
        A: "minecraft:amethyst_shard"
    });

    event.shaped("wizards:arcane_robe_feet",[
        ' A ',
        'EWE',
        ' A '
    ], {
        W: "wizards:wizard_robe_feet",
        E: "minecraft:ender_pearl",
        A: "minecraft:amethyst_shard"
    });

    event.shaped("wizards:fire_robe_head",[
        ' B ',
        'RWR',
        ' B '
    ], {
        W: "wizards:wizard_robe_head",
        B: "minecraft:blaze_powder",
        R: "minecraft:redstone"
    });

    event.shaped("wizards:fire_robe_chest",[
        ' B ',
        'RWR',
        ' B '
    ], {
        W: "wizards:wizard_robe_chest",
        B: "minecraft:blaze_powder",
        R: "minecraft:redstone"
    });

    event.shaped("wizards:fire_robe_legs",[
        ' B ',
        'RWR',
        ' B '
    ], {
        W: "wizards:wizard_robe_legs",
        B: "minecraft:blaze_powder",
        R: "minecraft:redstone"
    });

    event.shaped("wizards:fire_robe_feet",[
        ' B ',
        'RWR',
        ' B '
    ], {
        W: "wizards:wizard_robe_feet",
        B: "minecraft:blaze_powder",
        R: "minecraft:redstone"
    });

    event.shaped("wizards:frost_robe_head",[
        ' SP',
        'FWF',
        'PS '
    ], {
        W: "wizards:wizard_robe_head",
        S: "minecraft:snowball",
        F: "runes:frost_stone",
        P: "minecraft:prismarine_shard"
    });

    event.shaped("wizards:frost_robe_chest",[
        ' SP',
        'FWF',
        'PS '
    ], {
        W: "wizards:wizard_robe_chest",
        S: "minecraft:snowball",
        F: "runes:frost_stone",
        P: "minecraft:prismarine_shard"
    });

    event.shaped("wizards:frost_robe_legs",[
        ' SP',
        'FWF',
        'PS '
    ], {
        W: "wizards:wizard_robe_legs",
        S: "minecraft:snowball",
        F: "runes:frost_stone",
        P: "minecraft:prismarine_shard"
    });

    event.shaped("wizards:frost_robe_feet",[
        ' SP',
        'FWF',
        'PS '
    ], {
        W: "wizards:wizard_robe_feet",
        S: "minecraft:snowball",
        F: "runes:frost_stone",
        P: "minecraft:prismarine_shard"
    });
});