const CLOAKED_MODS = {
    computercraft: {
        "computer_advanced": {
            id: "techreborn:advanced_machine_casing",
            name: "§7A machine too complicated for you to comprehend...",
            blockStates: {
                facing: ["north", "west", "south", "east"],
                state: ["on", "blinking", "off"]
            }
        },
        "computer_normal": {
            id: "techreborn:advanced_machine_casing",
            name: "§7A machine too complicated for you to comprehend...",
            blockStates: {
                facing: ["north", "west", "south", "east"],
                state: ["on", "blinking", "off"]
            }
        },
    },
    betterarcheology: {
        "archeology_table": {
            id: "minecraft:crafting_table",
            name: "§7A working table unintelligible to you...",
            blockStates: {}
        },
        "torrent_totem": {
            itemId: "minecraft:gold_nugget",
            name: "§7A weird looking piece of gold..."
        },
        "soul_totem": {
            itemId: "minecraft:gold_nugget",
            name: "§7A weird looking piece of gold..."
        },
        "growth_totem": {
            itemId: "minecraft:gold_nugget",
            name: "§7A weird looking piece of gold..."
        },
        "radiance_totem": {
            itemId: "minecraft:gold_nugget",
            name: "§7A weird looking piece of gold..."
        },
        "iron_brush": {
            itemId: "minecraft:brush",
            name: "§7A weird looking brush...",
        },
        "diamond_brush": {
            itemId: "minecraft:brush",
            name: "§7A weird looking brush...",
        },
        "bomb": {
            itemId: "minecraft:dried_kelp",
            name: "§7No, it's not a bomb.",
        }
    },
    techreborn: {
        "tin_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_tin_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "lead_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_lead_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "silver_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_silver_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "galena_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_galena_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "bauxite_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_bauxite_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "ruby_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_ruby_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "sapphire_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_sapphire_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "iridium_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_iridium_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "cinnabar_ore": {
            id: "minecraft:netherrack",
            name: "block.minecraft.netherrack",
            blockStates: {}
        },
        "pyrite_ore": {
            id: "minecraft:netherrack",
            name: "block.minecraft.netherrack",
            blockStates: {}
        },
        "sphalerite_ore": {
            id: "minecraft:netherrack",
            name: "block.minecraft.netherrack",
            blockStates: {}
        },
        "peridot_ore": {
            id: "minecraft:end_stone",
            name: "block.minecraft.end_stone",
            blockStates: {}
        },
        "sheldonite_ore": {
            id: "minecraft:end_stone",
            name: "block.minecraft.end_stone",
            blockStates: {}
        },
        "sodalite_ore": {
            id: "minecraft:end_stone",
            name: "block.minecraft.end_stone",
            blockStates: {}
        },
        "tungsten_ore": {
            id: "minecraft:end_stone",
            name: "block.minecraft.end_stone",
            blockStates: {}
        },
        "deepslate_peridot_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "deepslate_sheldonite_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "deepslate_sodalite_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "deepslate_tungsten_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        }
    },
    bewitchment: {
        "salt_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_salt_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
        "silver_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_silver_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        }
    },
    create: {
        "zinc_ore": {
            id: "minecraft:stone",
            name: "block.minecraft.stone",
            blockStates: {}
        },
        "deepslate_zinc_ore": {
            id: "minecraft:deepslate",
            name: "block.minecraft.deepslate",
            blockStates: {}
        },
    },
    botania: {
        "white_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "orange_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "magenta_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "light_blue_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "yellow_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "lime_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "pink_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "gray_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "light_gray_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "cyan_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "purple_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "blue_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "brown_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "green_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "red_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },
        "black_mystical_flower": {
            id: "minecraft:poppy",
            name: "block.minecraft.poppy",
            blockStates: {}
        },

        "white_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "orange_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "magenta_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "light_blue_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "yellow_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "lime_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "pink_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "gray_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "light_gray_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "cyan_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "purple_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "blue_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "brown_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "green_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "red_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },
        "black_double_flower": {
            id: "minecraft:rose_bush",
            name: "block.minecraft.rose_bush",
            blockStates: {
                half: ["lower", "upper"]
            }
        },

        "white_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "orange_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "magenta_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "light_blue_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "yellow_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "lime_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "pink_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "gray_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "light_gray_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "cyan_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "purple_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "blue_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "brown_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "green_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "red_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
        "black_mushroom": {
            id: "minecraft:red_mushroom",
            name: "block.minecraft.red_mushroom",
            blockStates: {}
        },
    },
    ae2: {
        "sky_stone_block": {
            id: "minecraft:obsidian",
            name: "§7A weird and durable stone...",
            blockStates: {}
        }
    },
    farmersdelight: {
        "wild_cabbages": {
            id: "farmersdelight:sandy_shrub",
            name: "§7Slightly different looking grass...",
            blockStates: {}
        },
        "wild_onions": {
            id: "farmersdelight:sandy_shrub",
            name: "§7Slightly different looking grass...",
            blockStates: {}
        },
        "wild_tomatoes": {
            id: "farmersdelight:sandy_shrub",
            name: "§7Slightly different looking grass...",
            blockStates: {}
        },
        "wild_carrots": {
            id: "farmersdelight:sandy_shrub",
            name: "§7Slightly different looking grass...",
            blockStates: {}
        },
        "wild_potatoes": {
            id: "farmersdelight:sandy_shrub",
            name: "§7Slightly different looking grass...",
            blockStates: {}
        },
        "wild_beetroots": {
            id: "farmersdelight:sandy_shrub",
            name: "§7Slightly different looking grass...",
            blockStates: {}
        }
    },
    artifacts: {
        umbrella: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        everlasting_beef: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        eternal_steak: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        plastic_drinking_hat: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        novelty_drinking_hat: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        snorkel: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        night_vision_goggles: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        villager_hat: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        superstitious_hat: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        cowboy_hat: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        anglers_hat: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        lucky_scarf: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        scarf_of_invisibility: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        cross_necklace: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        panic_necklace: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        shock_pendant: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        flame_pendant: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        thorn_pendant: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        charm_of_sinking: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        cloud_in_a_bottle: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        obsidian_skull: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        antidote_vessel: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        universal_attractor: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        crystal_heart: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        helium_flamingo: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        chorus_totem: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        digging_claws: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        feral_claws: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        power_glove: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        fire_gauntlet: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        pocket_piston: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        vampiric_glove: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        golden_hook: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        onion_ring: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        pickaxe_heater: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        aqua_dashers: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        bunny_hoppers: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        kitty_slippers: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        running_shoes: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        snowshoes: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        steadfast_spikes: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        flippers: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        },
        rooted_boots: {
            itemId: "bewitchment:mending_sigil",
            name: "§7You can't even begin to understand this..."
        }
    },
    betterend: {
        ender_ore: {
            id: "minecraft:end_stone",
            name: "block.minecraft.end_stone",
            blockStates: {}
        },
        amber_ore: {
            id: "minecraft:end_stone",
            name: "block.minecraft.end_stone",
            blockStates: {}
        },
        thallasium_ore: {
            id: "minecraft:end_stone",
            name: "block.minecraft.end_stone",
            blockStates: {}
        }
    },
    scriptor: {
        spellbook: {
            itemId: "minecraft:book",
            name: "§7A weird book..."
        },
        tome_tier1: {
            itemId: "minecraft:book",
            name: "§7A weird book..."
        },
        tome_tier2: {
            itemId: "minecraft:book",
            name: "§7A weird book..."
        },
        tome_tier3: {
            itemId: "minecraft:book",
            name: "§7A weird book..."
        },
        tome_tier4: {
            itemId: "minecraft:book",
            name: "§7A weird book..."
        },
        scrap_tier1: {
            itemId: "minecraft:paper",
            name: "§7A weird piece of paper..."
        },
        scrap_tier2: {
            itemId: "minecraft:paper",
            name: "§7A weird piece of paper..."
        },
        scrap_tier3: {
            itemId: "minecraft:paper",
            name: "§7A weird piece of paper..."
        },
        identify_scroll: {
            itemId: "minecraft:paper",
            name: "§7A weird scroll..."
        }
    }
}

var ADVANCEMENTS = [];

AdvJSEvents.advancement(event => {
    let { CONDITION, PREDICATE, PROVIDER, TRIGGER } = event;

    let impossible = TRIGGER.impossible();

    const root = event.create("modpack:questing")
        .display(disp => {
            disp.setTitle("Questing");
            disp.setDescription(Component.join(
                "All internally used advancements for the quests. Press [", 
                Component.keybind("key.ftbquests.quests"),
                "] to open the quests!"
                ));
            disp.setIcon("additionaladditions:gilded_netherite_sword");
            disp.setAnnounceToChat(false);
            disp.setHidden(false);
            disp.setShowToast(false);
        })
        .criteria(crit => {
            crit.add("impossible", impossible);
        });
    
    for (let [mod, blocks] of Object.entries(CLOAKED_MODS)) {
        let c = root.addChild(mod, child => {
            child.display(disp => {
                    disp.setTitle(mod);
                    disp.setIcon("ftbquests:book");
                })
                .criteria(crit => { crit.add("impossible", impossible); });
        });
        for (let [id, props] of Object.entries(blocks)) {
            ADVANCEMENTS.push("modpack:questing/"+mod+"/"+id);
            c.addChild(mod+"/"+id, child => {
                child.display(disp => {
                    disp.setTitle(mod+":"+id);
                    disp.setIcon(props.id);
                    disp.setAnnounceToChat(false);
                    disp.setHidden(false);
                    disp.setShowToast(false);
                }).criteria(crit => { crit.add("impossible", impossible); });
            });
        }
    }
});

function cloakBlock(item, blockId, blockStates, name) {
    let states = [""];
    for (let [type, vals] of Object.entries(blockStates)) {
        let newStates = [];
        for (let state of states) {
            for (let val of vals) {
                newStates.push(state+","+type+"="+val);
            }
        }
        states = newStates;
    }
    states = states.map((val, ind, arr) => item+"["+val.substring(1)+"]");
    let map = {};
    let nameMap = {};
    for (let state of states) {
        map[state] = blockId;
        nameMap[state] = name;
    }
    return { blocks: map, names: nameMap };
}

ServerEvents.revelation(event => {
    for (let [mod, blocks] of Object.entries(CLOAKED_MODS)) {
        for (let [id, props] of Object.entries(blocks)) {
            let item = mod+":"+id;
            let blockMap = {};
            let itemMap = {};
            let blockNameMap = {};
            let itemNameMap = {};
            if ('id' in props) {
                itemMap[item] = props.id;
                itemNameMap[item] = props.name;
                ({blocks: blockMap, names: blockNameMap} = cloakBlock(item, props.id, props.blockStates, props.name));
            } else {
                if ('itemId' in props) {
                    itemMap[item] = props.itemId;
                    itemNameMap[item] = props.name;
                }
                if ('blockId' in props) {
                    ({blocks: blockMap, names: blockNameMap} = cloakBlock(item, props.blockId, props.blockState, props.names));
                }
            }
            let json = {
                "block_states": blockMap,
                "items": itemMap,
                "item_name_replacements": itemNameMap,
                "block_name_replacements": blockNameMap,
                "advancement": "modpack:questing/"+mod+"/"+id
            };
            event.registerFromJson(json);
        }
    }
});

function itemRightClick(event) {
    let components = event.item.id.split(":");
    let mod = components[0];
    let item = components[1];
    let adv = `modpack:questing/${mod}/${item}`;
    if (ADVANCEMENTS.indexOf(adv) !== -1) {
        let name = event.player.name.string;
        if (event.server.runCommandSilent(`execute as @e[type=player, name=${name}, advancements={${adv}=true}] run return 1`) < 1) {
            event.cancel()
        }
    }
}

ItemEvents.rightClicked(itemRightClick);
BlockEvents.rightClicked(itemRightClick);
ItemEvents.entityInteracted(itemRightClick);

PlayerEvents.tick(event => {
    if (!event.player.getStages().has("vanilla+")) {
        if (event.player.mainHandItem.id.startsWith("mcdw:")) {
            event.server.runCommandSilent(`execute at ${event.player.name.string} run summon item ~ ~ ~ {Item:${event.player.mainHandItem.save({})}}`);
            event.server.runCommandSilent(`item replace entity ${event.player.name.string} hotbar.${event.player.selectedSlot} with air`);
        }
    }
});

ItemEvents.canPickUp(event => {
    if (event.item.id.startsWith("mcdw:") && !event.player.getStages().has("vanilla+")) {
        event.player.setStatusMessage("You seem to not be able to handle this weapon...");
        event.cancel();
    }
});

const TECH_UNALLOWED_BLOCKS = [
    "minecraft:enchanting_table",
    "minecraft:brewing_stand",
    "mcde:runic_table",
    "mcde:roll_bench",
    "mcde:gilding_foundry",
    "davespotioneering:compound_brewing_stand",
    "davespotioneering:potion_injector"
];

BlockEvents.rightClicked(event => {
    let stages = event.player.getStages();
    if ((!stages.has("vanilla+")) && (!stages.has("magic")) && TECH_UNALLOWED_BLOCKS.indexOf(event.block.id) !== -1) {
        event.player.setStatusMessage("You don't understand how this block works...");
        event.cancel();
    }
});